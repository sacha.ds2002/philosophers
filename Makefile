# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/02/21 13:20:16 by sada-sil          #+#    #+#              #
#    Updated: 2023/09/06 14:17:37 by sada-sil         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = 			philo

CC = 			gcc
CFLAGS = 		-Wall -Werror -Wextra
RM = 			rm -rf

SRCS =			main.c \
				src/philo_utils.c \
				src/parsing_utils.c \
				src/philo_utils_2.c \
				src/initialize_threads.c \
				src/threads.c \
				src/print_utils.c \
				src/printers.c \
				src/threads_utils.c

OBJS = $(SRCS:.c=.o)

${NAME}:	${OBJS}
	${CC} ${CFLAGS} ${OBJS} -o ${NAME}

all:	${NAME}

clean:
	${RM} ${OBJS}

fclean: clean
	${RM} ${NAME}

re: fclean all

.PHONY: all clean fclean re