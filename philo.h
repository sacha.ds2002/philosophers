/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/05 11:53:44 by sada-sil          #+#    #+#             */
/*   Updated: 2023/09/06 13:34:36 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <stdio.h>
# include <pthread.h>
# include <stdlib.h>
# include <sys/time.h>
# include <unistd.h>

typedef struct s_death
{
	int				deaths;
	pthread_mutex_t	mutex;
}	t_death;

typedef struct s_fork
{
	int				taken;
	pthread_mutex_t	mutex;
}	t_fork;

typedef struct s_datas
{
	long			philo_number;
	long			time_to_die;
	long			time_to_eat;
	long			time_to_sleep;
	long			max_eat;
	t_death			*death;
}	t_datas;

typedef struct s_philo
{
	t_death			*death;
	t_fork			*fork;
	int				state;
	int				id;
	long			time_to_die;
	long			time_to_eat;
	long			time_to_sleep;
	long			max_eat;
	long			start_time;
	int				times_eaten;
	long			last_time;
	void			*right_philo;
}	t_philo;

t_datas	*ft_parse_params(int ac, char **av);
t_philo	*ft_create_philo(int id, t_datas *datas);
t_philo	*ft_last_philo(t_philo *philo);
void	ft_philo_add(t_philo **philo, t_philo *new);
void	*philo_thread(void *arg);
void	init_threads(t_datas *datas);
long	ft_time_diff(long last_time);
long	ft_time_elapsed(long start_time);
long	ft_get_time(void);
void	*throw_error(char *str);
long	ft_atol(const char *str);
void	ft_check_digits(int ac, char **av);
int		check_death(t_philo *philo);
int		ft_take_fork(t_philo *philo, t_fork *fork, int id);
int		ft_release_fork(t_fork *fork, int id);
int		ft_check_fork(t_fork *fork);

//print utils
char	*ft_get_color(int id);
void	printf_think(t_philo *philo);
void	printf_sleep(t_philo *philo);
void	printf_eat(t_philo *philo);
void	printf_die(t_philo *philo);
void	printf_fork(t_philo *philo);

#endif