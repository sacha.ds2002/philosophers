/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_utils_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/12 09:52:50 by sada-sil          #+#    #+#             */
/*   Updated: 2023/06/01 16:57:37 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../philo.h"

long	ft_get_time(void)
{
	struct timeval	current_time;

	gettimeofday(&current_time, NULL);
	return (current_time.tv_sec * 1000 + current_time.tv_usec / 1000);
}

long	ft_time_elapsed(long start_time)
{
	struct timeval	current_time;
	long			diff;

	gettimeofday(&current_time, NULL);
	diff = (current_time.tv_sec * 1000)
		+ current_time.tv_usec / 1000 - start_time;
	if (diff < 0)
		diff *= -1;
	return (diff);
}

long	ft_time_diff(long last_time)
{
	struct timeval	current_time;
	long			diff;

	gettimeofday(&current_time, NULL);
	diff = (current_time.tv_sec * 1000)
		+ current_time.tv_usec / 1000 - last_time;
	if (diff < 0)
		diff *= -1;
	return (diff);
}

int	check_death(t_philo *philo)
{
	int	ret;

	ret = 0;
	pthread_mutex_lock(&philo->death->mutex);
	if (philo->death->deaths > 0)
		ret = -1;
	pthread_mutex_unlock(&philo->death->mutex);
	return (ret);
}
