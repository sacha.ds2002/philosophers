/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   threads_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 10:30:50 by sada-sil          #+#    #+#             */
/*   Updated: 2023/09/06 13:31:45 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../philo.h"

int	ft_check_fork(t_fork *fork)
{
	int	ret;

	ret = 0;
	if (!fork)
		return (1);
	pthread_mutex_lock(&fork->mutex);
	if (fork->taken != 0)
		ret = 1;
	pthread_mutex_unlock(&fork->mutex);
	return (ret);
}

int	ft_take_fork(t_philo *philo, t_fork *fork, int id)
{
	int	ret;

	ret = 0;
	pthread_mutex_lock(&fork->mutex);
	if (fork->taken != id)
	{
		if (fork->taken == 0)
		{
			fork->taken = id;
			printf_fork(philo);
		}
		else
			ret = 1;
	}
	pthread_mutex_unlock(&fork->mutex);
	return (ret);
}

int	ft_release_fork(t_fork *fork, int id)
{
	int	ret;

	ret = 0;
	pthread_mutex_lock(&fork->mutex);
	if (fork->taken == id)
		fork->taken = 0;
	else
		ret = 1;
	pthread_mutex_unlock(&fork->mutex);
	return (ret);
}
