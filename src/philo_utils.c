/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/09 14:09:14 by sada-sil          #+#    #+#             */
/*   Updated: 2023/09/06 15:00:54 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../philo.h"

void	*throw_error(char *str)
{
	printf("%s\n", str);
	return (NULL);
}

t_philo	*ft_last_philo(t_philo *philo)
{
	if (philo == NULL)
		return ((t_philo *)0);
	while (philo && philo->right_philo != NULL)
		philo = philo->right_philo;
	return (philo);
}

void	ft_philo_add(t_philo **philo, t_philo *new)
{
	t_philo	*last_philo;

	if (*philo == NULL)
		*philo = new;
	else
	{
		last_philo = ft_last_philo(*philo);
		if (last_philo != NULL)
			last_philo->right_philo = new;
	}
}

t_philo	*ft_create_philo(int id, t_datas *datas)
{
	t_philo	*philo;

	philo = (t_philo *)malloc(sizeof(t_philo));
	philo->id = id;
	philo->state = 3;
	philo->times_eaten = 0;
	philo->time_to_die = datas->time_to_die;
	philo->time_to_eat = datas->time_to_eat;
	philo->time_to_sleep = datas->time_to_sleep;
	philo->max_eat = datas->max_eat;
	philo->death = datas->death;
	philo->start_time = ft_get_time();
	philo->last_time = philo->start_time;
	philo->fork = (t_fork *)malloc(sizeof(t_fork));
	philo->right_philo = NULL;
	philo->fork->taken = 0;
	if (pthread_mutex_init(&philo->fork->mutex, NULL) != 0)
		return (throw_error("The philo mutex couldn't be instanciated"));
	return (philo);
}

t_datas	*ft_parse_params(int ac, char **av)
{
	t_datas			*datas;

	ft_check_digits(ac, av);
	datas = (t_datas *)malloc(sizeof(t_datas));
	datas->philo_number = ft_atol(av[1]);
	if (datas->philo_number <= 0)
		return (throw_error("There should be at least one philosopher."));
	datas->time_to_die = ft_atol(av[2]);
	datas->time_to_eat = ft_atol(av[3]);
	datas->time_to_sleep = ft_atol(av[4]);
	if (ac == 6)
		datas->max_eat = ft_atol(av[5]);
	else
		datas->max_eat = -1;
	datas->death = (t_death *)malloc(sizeof(t_death));
	datas->death->deaths = 0;
	if (pthread_mutex_init(&datas->death->mutex, NULL) != 0)
		return (throw_error("The death mutex couldn't be instanciated"));
	return (datas);
}
