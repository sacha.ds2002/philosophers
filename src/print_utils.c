/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/01 10:50:43 by sada-sil          #+#    #+#             */
/*   Updated: 2023/06/02 12:50:03 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../philo.h"

char	*ft_get_color(int id)
{
	if (id % 7 == 0)
		return ("\033[0;37m");
	else if (id % 6 == 0)
		return ("\033[0;36m");
	else if (id % 5 == 0)
		return ("\033[1;35m");
	else if (id % 4 == 0)
		return ("\033[0;34m");
	else if (id % 3 == 0)
		return ("\033[1;33m");
	else if (id % 2 == 0)
		return ("\033[0;32m");
	else
		return ("\033[0;31m");
}
