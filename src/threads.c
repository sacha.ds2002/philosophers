/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   threads.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/10 14:34:39 by sada-sil          #+#    #+#             */
/*   Updated: 2023/09/07 10:53:58 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../philo.h"

static void	ft_sleep(t_philo *philo)
{
	printf_sleep(philo);
	while (ft_time_diff(philo->last_time)
		<= (philo->time_to_eat + philo->time_to_sleep)
		&& ft_time_diff(philo->last_time) <= philo->time_to_die)
		usleep(500);
	philo->state = 2;
}

static void	ft_eat(t_philo *philo, t_fork *right_fork)
{
	long	time_diff;

	if (check_death(philo) != -1 && ft_check_fork(right_fork) == 0
		&& ft_take_fork(philo, philo->fork, philo->id) == 0)
	{
		if (ft_take_fork(philo, right_fork, philo->id) == 0)
		{
			printf_eat(philo);
			philo->last_time = ft_get_time();
			time_diff = 0;
			while (time_diff <= philo->time_to_eat
				&& time_diff <= philo->time_to_die)
			{
				time_diff = ft_time_diff(philo->last_time);
				usleep(500);
			}
			philo->state = 1;
			philo->times_eaten++;
			ft_release_fork(philo->fork, philo->id);
			ft_release_fork(right_fork, philo->id);
		}
	}
}

static void	ft_think(t_philo *philo)
{
	printf_think(philo);
	philo->state = 0;
}

static void	routine(long time_diff, t_philo *philo, t_fork *r_fork)
{
	while (1)
	{
		if (time_diff >= philo->time_to_die
			|| (philo->max_eat > 0 && philo->times_eaten >= philo->max_eat)
			|| check_death(philo) == -1)
			break ;
		if ((philo->state == 0 || philo->state == 3)
			&& check_death(philo) != -1)
			ft_eat(philo, r_fork);
		else if (philo->state == 1 && check_death(philo) != -1)
			ft_sleep(philo);
		else if (philo->state == 2 && check_death(philo) != -1)
			ft_think(philo);
		time_diff = ft_time_diff(philo->last_time);
	}
}

void	*philo_thread(void *arg)
{
	t_philo	*philo;
	t_fork	*r_fork;
	long	time_diff;

	time_diff = 0;
	philo = (t_philo *)arg;
	if (philo->right_philo)
		r_fork = ((t_philo *)philo->right_philo)->fork;
	else
		r_fork = NULL;
	routine(time_diff, philo, r_fork);
	pthread_mutex_lock(&philo->death->mutex);
	if (philo->death->deaths == 0)
	{
		if (philo->max_eat < 0 || philo->times_eaten < philo->max_eat)
		{
			printf_die(philo);
			philo->death->deaths = 1;
		}	
	}
	pthread_mutex_unlock(&philo->death->mutex);
	pthread_exit("dead");
}
