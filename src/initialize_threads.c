/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_threads.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/10 11:42:52 by sada-sil          #+#    #+#             */
/*   Updated: 2023/09/06 15:06:54 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../philo.h"

static t_philo	*create_philos(t_datas *datas)
{
	t_philo	*first_philo;
	t_philo	*last_philo;
	int		i;

	i = 1;
	first_philo = ft_create_philo(i, datas);
	if (!first_philo)
		return ((t_philo *)0);
	while (++i <= datas->philo_number)
	{
		last_philo = ft_create_philo(i, datas);
		if (!last_philo)
			return ((t_philo *)0);
		ft_philo_add(&first_philo, last_philo);
	}
	if (last_philo != NULL)
		last_philo->right_philo = first_philo;
	return (first_philo);
}

static void	clear_all(t_philo *philo, t_datas *datas)
{
	t_philo	*tmp;
	int		i;

	i = 0;
	while (++i < datas->philo_number)
	{
		tmp = philo->right_philo;
		pthread_mutex_destroy(&philo->death->mutex);
		pthread_mutex_destroy(&philo->fork->mutex);
		free(philo->fork);
		free(philo);
		philo = tmp;
	}
	pthread_mutex_destroy(&philo->death->mutex);
	pthread_mutex_destroy(&philo->fork->mutex);
	free(philo->fork);
	free(philo);
}

void	init_threads(t_datas *datas)
{
	t_philo		*philo;
	pthread_t	thid;
	void		*ret;

	ret = NULL;
	philo = create_philos(datas);
	if (!philo)
	{
		pthread_mutex_destroy(&datas->death->mutex);
		free(philo);
		return ;
	}
	while (philo->id <= datas->philo_number)
	{
		pthread_create(&thid, NULL, philo_thread, philo);
		if (philo->id == datas->philo_number || datas->philo_number <= 1)
			break ;
		philo = philo->right_philo;
		usleep(100);
	}
	if (datas->philo_number > 1)
		philo = philo->right_philo;
	pthread_join(thid, ret);
	clear_all(philo, datas);
}
