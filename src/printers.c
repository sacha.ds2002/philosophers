/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printers.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/01 16:26:36 by sada-sil          #+#    #+#             */
/*   Updated: 2023/09/06 15:16:30 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../philo.h"

void	printf_fork(t_philo *philo)
{
	printf("%s%ld ms\t\tPhilo: %d\t\thas taken a fork.\n",
		ft_get_color(philo->id),
		ft_time_elapsed(philo->start_time), philo->id);
}

void	printf_think(t_philo *philo)
{
	printf("%s%ld ms\t\tPhilo: %d\t\tis thinking.\n",
		ft_get_color(philo->id),
		ft_time_elapsed(philo->start_time), philo->id);
}

void	printf_sleep(t_philo *philo)
{
	printf("%s%ld ms\t\tPhilo: %d\t\tis sleeping.\n",
		ft_get_color(philo->id),
		ft_time_elapsed(philo->start_time), philo->id);
}

void	printf_eat(t_philo *philo)
{
	printf("%s%ld ms\t\tPhilo: %d\t\tis eating.\n",
		ft_get_color(philo->id),
		ft_time_elapsed(philo->start_time), philo->id);
}

void	printf_die(t_philo *philo)
{
	printf("%s%ld ms\t\tPhilo: %d\t\tdied.\n",
		ft_get_color(philo->id),
		ft_time_elapsed(philo->start_time), philo->id);
}
