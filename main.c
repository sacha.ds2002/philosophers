/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/05 11:53:34 by sada-sil          #+#    #+#             */
/*   Updated: 2023/09/06 15:04:15 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	main(int ac, char **av)
{
	t_datas	*datas;

	if (ac != 5 && ac != 6)
	{
		throw_error("il faut spécifier entre 4 et 5 paramètres.");
		return (0);
	}
	datas = ft_parse_params(ac, av);
	if (!datas)
		return (0);
	init_threads(datas);
	pthread_mutex_destroy(&datas->death->mutex);
	free(datas->death);
	free(datas);
	return (0);
}
